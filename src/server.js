
var fs = require('fs'),
    express = require('express'),
    https = require('https'),
    http = require('http');


var privateKey = fs.readFileSync('keys/privatekey.pem').toString(),
    certificate = fs.readFileSync('keys/server.crt').toString();

var httpsPort = 2013, httpPort = 2014;
	
var app = express();

app.use(express.static(__dirname));

var serverS = https.createServer({key: privateKey, cert: certificate}, app).listen(httpsPort);
var server = http.createServer(app).listen(httpPort);

console.log('running on https://localhost:'+ httpsPort + ' and http://localhost:' + httpPort);

var io = require('socket.io').listen(serverS);
var ioHttp = require('socket.io').listen(server);

var peerInfo = {},
	ANON_GUEST = 'anonymous_guest',
	MAX_NUM = 5;

io.sockets.on('connection', function (socket){
	
	console.log('>>>>>>>>>>>> Client connected -- id: ',  socket.id);
	socket.emit('connId', socket.id);
	
	peerInfo[socket.id] = {};
	
	function log(){
		var array = [">>> "];
	  for (var i = 0; i < arguments.length; i++) {
	  	array.push(arguments[i]);
	  }
	    socket.emit('log', array);
	}

	function createRoom(roomName, peerName){
		var numClients = io.sockets.clients(roomName).length;
		
		console.log('>>>>>>>>>>>> Check for room: ', roomName);
		console.log('Room ' + roomName + ' has ' + numClients + ' client(s)');

		if (numClients == 0){
			log('Request to create room: ', roomName);
			socket.join(roomName);
			socket.emit('created', roomName);
			socket.emit('owner', true);
		} else {
			socket.emit('nameTaken', roomName);
		}
		
		peerInfo[socket.id].room = roomName;
		peerInfo[socket.id].peerName = peerName || ANON_GUEST;
		console.log('Client ' + socket.id + ' created room ' + roomName);
		console.log('<<<<<<<<<<<<<<<<<<<<<<<<<<<<');
	}
	
	function sendOffer(roomName, sdpData) {
		//TODO do not send to already connected peers
		console.log('Send to unconnected peers in room: '+ roomName);
		io.sockets.socket(sdpData.remotePeerId).emit('gotOffer', sdpData);
	}
	
	function sendAnswer(roomName, sdpData) {
		//TODO do not send to already connected peers
		console.log('Send to ANSWER unconnected peer in room: '+ roomName);
		io.sockets.socket(sdpData.remotePeerId).emit('gotAnswer', sdpData);
	}
	
	socket.on('message', function (message) {
		log('>>>>>>>>>>>> Got message: ', message);
		socket.broadcast.to(roomName).emit('message', message);
	});
	
	socket.on('emitIceCandidate', function (roomName, data) {
		console.log('>>>>>>>>>>>> Got ice candidate: ' + data + ' In room [' + roomName + ']');
		io.sockets.socket(data.remotePeerId).emit('gotIceCandidate', data);
	});
	
	//create a chat room
	socket.on('create', function(roomName, peerName){ 
		createRoom(roomName, peerName); 
	});
	
	// offer to exchange sdp data
	socket.on('offer', function(roomName, sdpData){ 
		console.log('>>>>>>>>>>>> In room [' + roomName +'], to: '+ sdpData.remotePeerId +' -- offer recevied: ' + sdpData);
		sendOffer(roomName, sdpData); 
	});
	
	socket.on('answer', function(roomName, sdpData){ 
		console.log('>>>>>>>>>>>> In room [' + roomName +'] ANSWER recevied: ' + sdpData);
		sendAnswer(roomName, sdpData); 
	});
	
	socket.on('joinRoom', function (req) {
		var room = io.sockets.clients(req.roomName);
		
		if(room==null){
			console.log('Room does not exist: ', req.roomName);
			socket.emit('roomDNE', req.roomName);
			return;
		}
		
		var numClients = room.length;
		console.log('Request to join room', req.roomName);
		console.log('Room ' + req.roomName + ' has ' + numClients + ' client(s)');

		if (numClients < MAX_NUM || true) { //TODO add later
			io.sockets.in(req.roomName).emit('join', req.roomName);
			socket.join(req.roomName);
			socket.broadcast.to(req.roomName).emit('newRoomPeer', req.peerName);
			socket.emit('joinRoomOk', req.roomName);
		} else {
			socket.emit('full', req.roomName);
		}
		peerInfo[socket.id].room = req.roomName;
		peerInfo[socket.id].peerName = req.peerName || ANON_GUEST;
		console.log('Client [' + socket.id + '] joined room ' + req.roomName);
	});
	
	// Join multi-part conference calls
	socket.on('joinConference', function (req) {
		var room = io.sockets.clients(req.roomName);
		
		if(room==null){
			console.log('Room does not exist: ', req.roomName);
			socket.emit('roomDNE', req.roomName);
			return;
		}
		
		var numClients = room.length;
		console.log('Request to join room', req.roomName);
		console.log('Room ' + req.roomName + ' has ' + numClients + ' client(s)');
		
		if (numClients == 0 ) {
			io.sockets.socket(socket.id).emit('emptyRoom', req.roomName);
		}
		else if (numClients < MAX_NUM || true) { //TODO add room limit later
			socket.broadcast.to(req.roomName).emit('newConfPeer', req.peerId);
		} else {
			io.sockets.socket(socket.id).emit('full', req.roomName);
		}
	});
	
	socket.on('leaveRoom', function (peerName) {
		var roomName = peerInfo[socket.id].roomName;
		socket.broadcast.to(roomName).emit('peerLeft', socket.id, socket.id, peerInfo[socket.id].peerName);
		peerInfo[socket.id].roomName = null;
		console.log('client ' + socket.id + ' left room: ' + roomName);
	});
	
	socket.on('disconnect', function () {
		var roomName = peerInfo[socket.id].roomName;
		socket.broadcast.to(roomName).emit('peerLeft', socket.id, peerInfo[socket.id].peerName);
		delete peerInfo[socket.id];
		console.log('client ' + socket.id + ' disconnected');
	});
	
});