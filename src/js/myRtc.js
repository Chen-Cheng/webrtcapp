/// /////////////////////////////////////////////////////////
// - MyRtc
// /////////////////////////////////////////////////////////

function MyRtc (){
	var rtc = this;
	    
	//some predefined configuration
	var pcConfig = webrtcDetectedBrowser === 'firefox' ?
	  {'iceServers':[{'url':'stun:23.21.150.121'}]} : // number IP
	  {'iceServers': [{'url': 'stun:stun.l.google.com:19302'}]};

	var pcConstraints = {
	  'optional': [
		{'DtlsSrtpKeyAgreement': true},
		{'RtpDataChannels': true}
	  ]};

	
	var sdpConstraints = {'mandatory': {
                      'OfferToReceiveAudio': true,
                      'OfferToReceiveVideo': true }};
					  
	rtc.signalClient = new ChatRoomClient();
	rtc.signalChannel = rtc.signalClient.getSignalChannel();
	
	rtc.mediaHelper = new MediaHelper();
	rtc.sdpHelper = new SdpHelper();
	
	rtc.peers = {};
	rtc.myId;
	
	var isStarted = false;
	var sessionDesc, remoteStream;
	
	var onRemoteStreamAdded = function (peerId, event) {
		handleRemoteStreamAdded(peerId, event);
	};

	var onRemoteStreamRemoved = function (event) {
		//console.log('Remote stream removed: ' + event.stream.id);
	};

	var onSignalingStateChanged = function (event) {
	  updateInfoDiv(event);
	}

	var onIceConnectionStateChanged = function (event) {
	  updateInfoDiv(event);
	}
	
	function updateInfoDiv(event){
		//console.log(event);
	}
	
	function mergeConstraints(cons1, cons2) {
	  var merged = cons1;
	  for (var name in cons2.mandatory) {
		merged.mandatory[name] = cons2.mandatory[name];
	  }
	  merged.optional.concat(cons2.optional);
	  return merged;
	}
	
	function iceCandidateType(candidateSDP) {
	  if (candidateSDP.indexOf("typ relay ") >= 0)
		return "TURN";
	  if (candidateSDP.indexOf("typ srflx ") >= 0)
		return "STUN";
	  if (candidateSDP.indexOf("typ host ") >= 0)
		return "HOST";
	  return "UNKNOWN";
	}

	function onIceCandidate(remotePeerId, event) {
	  if (event.candidate) {
		var data = {
			peerId : rtc.myId,
			remotePeerId: remotePeerId,
			icePkg: {type: 'candidate',
					 label: event.candidate.sdpMLineIndex,
					 id: event.candidate.sdpMid,
					 candidate: event.candidate.candidate},
			type: 'candidate'
		};
	  
		rtc.signalClient.emitIceCandidate(data);
	  } else {
		console.log('End of candidates.');
	  }
	}
	
	var newPeerpackage = function(id, payload) {
		return { peerId: id, payload: payload };
	};
	
	function onSetSessionDescriptionSuccess() {
		//console.log('Set session description success.');
	}

	function onSetSessionDescriptionError(error) {
	  //console.error('Failed to set session description: ' + error.toString());
	}
	
	function handleRemoteStreamAdded(peerId, event) {
		//console.log('Remote stream added.');
		rtc.mediaHelper.addRemoteStream(peerId, event.stream);
	}
	
	var __constructor = function(iRtc){
		iRtc.mediaHelper.doGetUserMedia();
	}(this);
	
	////////////////Public functions
	
	MyRtc.prototype.onRcvIceCandidate = function(data) {
		var candidate = new RTCIceCandidate({sdpMLineIndex: data.icePkg.label,
											 candidate:data.icePkg.candidate});
		var pc = rtc.peers[data.peerId];
		pc.addIceCandidate( candidate, 
							function(e){console.info('Add candidate OK');},
							function(e){console.error('Add candidate Error: ' + e);});
		return rtc;
	};
	
	MyRtc.prototype.makeOffer = function (peerId) {
	  var pc = rtc.maybeStart(peerId);
	  var constraints = {'optional': [], 'mandatory': {'MozDontOfferDataChannel': true}};
	  //temporary measure to remove Moz* constraints in Chrome
	  if (webrtcDetectedBrowser === 'chrome') {
		for (var prop in constraints.mandatory) {
		  if (prop.indexOf('Moz') !== -1) {
			delete constraints.mandatory[prop];
		  }
		 }
	   }
	  constraints = mergeConstraints(constraints, sdpConstraints);
	  
	  pc.createOffer(setLocalSdpPkg.bind(undefined, pc, peerId), 
					function(e){
						console.error(e.message);
					}, 
					constraints);
	};
	
	function setLocalSdpPkg(pc, remotePeerId, sessionDescription){
		setLocalSessionDescription(pc, sessionDescription); 
		rtc.signalClient.makeOffer({peerId:rtc.myId, sessDescription: sessionDescription, remotePeerId: remotePeerId});
	}
	
	MyRtc.prototype.maybeStart = function(peerId) {	  
	  if (rtc.mediaHelper.localStream!=null) {
		var pc = rtc.createPeerConnection(peerId);
		pc.addStream(rtc.mediaHelper.localStream);
		rtc.peers[peerId] = pc;
		return pc;
	  }
	};
	
	MyRtc.prototype.createPeerConnection = function(peerId) {
		try {
			// Create an RTCPeerConnection via the polyfill (adapter.js).
			var pc = new RTCPeerConnection(pcConfig, pcConstraints);
			pc.onicecandidate = onIceCandidate.bind(undefined, peerId);
		  } catch (e) {
			alert('Cannot create RTCPeerConnection object; \
				  WebRTC is not supported by this browser.');
			return;
		  }
		pc.onaddstream = onRemoteStreamAdded.bind(undefined, peerId);
		pc.onremovestream = onRemoteStreamRemoved;
		pc.onsignalingstatechange = onSignalingStateChanged;
		pc.oniceconnectionstatechange = onIceConnectionStateChanged;
		return pc;
	};
	
	function setLocalSessionDescription(pc, sessionDescription) {
	  sessionDescription.sdp = rtc.sdpHelper.preferOpus(sessionDescription.sdp);
	  pc.setLocalDescription(sessionDescription, onSetSessionDescriptionSuccess, onSetSessionDescriptionError);
	  sessionDesc = sessionDescription;
	}
	
	MyRtc.prototype.makeAnswer = function (remotePeerId, rmtSessDesp) {
		var pc = rtc.maybeStart(remotePeerId);
		pc.setRemoteDescription(new RTCSessionDescription(rmtSessDesp));
		return pc;
	}
	
	MyRtc.prototype.doAnswer = function (data) {
		var pc = rtc.makeAnswer(data.peerId, data.sessDescription);
		console.log('Sending answer to peer.');
		pc.createAnswer(function(pc, sessDesp) {
							setLocalSessionDescription(pc, sessDesp);
							rtc.signalClient.makeAnswer({peerId: rtc.myId, remotePeerId: data.peerId, sessDescription: sessDesp});
						}.bind(undefined, pc),
						
						function(e){
							console.error(e.message);
						},
						
						sdpConstraints);
	}
	
	//make signaling SDP pkg
	MyRtc.prototype.getLocalSdp = function() {
		return sessionDesc;
	};
	
	MyRtc.prototype.setRemote  = function (data) {
	  // Set Opus in Stereo, if stereo enabled.
	  data.sessDescription.sdp = rtc.sdpHelper.preferOpus(data.sessDescription.sdp);
	  var pc = rtc.peers[data.peerId];
	  pc.setRemoteDescription(new RTCSessionDescription(data.sessDescription),  onSetRemoteDescriptionSuccess, onSetSessionDescriptionError);
	}
	
	function onSetRemoteDescriptionSuccess() {
		// By now all addstream events for the setRemoteDescription have fired.
		// So we can know if the peer is sending any stream or is only receiving.
		if (remoteStream) {
		  //TODO waitForRemoteVideo();
		} else {
		  console.log("Not receiving any stream.");
		  //transitionToActive();
		}
	}
	
	/*function waitForRemoteVideo() {
	  // Call the getVideoTracks method via adapter.js.
	  videoTracks = remoteStream.getVideoTracks();
	  if (videoTracks.length === 0 || remoteVideo.currentTime > 0) {
		transitionToActive();
	  } else {
		setTimeout(waitForRemoteVideo, 100);
	  }
	}
	
	function transitionToActive() {
	  reattachMediaStream(miniVideo, localVideo);
	  remoteVideo.style.opacity = 1;
	  setTimeout(function() { localVideo.src = ''; }, 500);
	  setTimeout(function() { miniVideo.style.opacity = 1; }, 1000);
	}*/
	
	MyRtc.prototype.hangUp = function(){
		rtc.mediaHelper.localStream.stop();
		for(var id in rtc.peers){
			var pc = rtc.peers[id];
			pc.close();
			pc = null;
			delete rtc.peers[id]
		}
		
		rtc.signalChannel.disconnect();
	};
	
	function handlePeerLeft(id, peerName){
		var pc = rtc.peers[id];
		if(pc==null){
			return;
		}
		pc.close();
		pc = null;
		delete rtc.peers[id];
		rtc.mediaHelper.removeRemoteStream(id, peerName);
	}
	
	//////////////////SIGNAL HANDLER
	
	rtc.signalChannel.on('connId', function(id){
		rtc.myId = id;
	});
	
	rtc.signalChannel.on('gotOffer', function(data){
		rtc.doAnswer(data);
	});
	
	// Received answers from peers
	rtc.signalChannel.on('gotAnswer', function(data){
		rtc.setRemote(data);
	});
	
	rtc.signalChannel.on('newConfPeer', function(peerId){ 
		//already in chat, should be fixed on the server side
		if(rtc.peers[peerId]!=null){
			return;
		}
		
		rtc.makeOffer(peerId);
	});
	
	rtc.signalChannel.on('gotIceCandidate', function(iceCandidateData){
		rtc.onRcvIceCandidate(iceCandidateData);
	});
	
	rtc.signalChannel.on('peerLeft', function(peerId, peerName){
		handlePeerLeft(peerId, peerName);
	});
}