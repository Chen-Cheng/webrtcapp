/// /////////////////////////////////////////////////////////
// - ChatRoomClient
//	 - Helper for setting the chat room
// /////////////////////////////////////////////////////////

function ChatRoomClient() {
	var crc = this;
	
	//TODO query from the server side
	var SIGNAL_SRV_URL = 'https://'+window.location.hostname+':2013';
	
	// Use io socket for connection
	var socket,
		msgQueue = [],
		signalingReady = false;
	
	crc.room = {};
	crc.peerName = '';
	
	var __construct = function(client){
		socket = new io.connect(SIGNAL_SRV_URL);
	}(this);
	
	//create room
	ChatRoomClient.prototype.createRoom = function(roomName){
		socket.emit('create', roomName, crc.peerName);
	};
	
	ChatRoomClient.prototype.leaveRoom = function(){
		socket.emit('leaveRoom', crc.peerName);
	};
	
	ChatRoomClient.prototype.handleOffer = function(message){
	  // Add offer to the beginning of msgQueue, since we can't handle
      // Early candidates before offer at present.
      msgQueue.unshift(msg);
      // Callee creates PeerConnection
      signalingReady = true;
      maybeStart();	
	};
	
	//@abstract
	ChatRoomClient.prototype.openChannel = function(){};
	
	socket.on('created', function(roomName){ 
		crc.onRoomCreated(roomName)
	});
	
	socket.on('newRoomPeer', function(peerId){
		showInfo(peerId + ' just joined the room.');
	});
	
	socket.on('full', function(){ 
		crc.onRoomFull()
	});
	
	socket.on('joinRoomOk', function(roomName){
		crc.room.roomName = roomName;
		enteredRoom("Joined room " + roomName);
	});
	
	ChatRoomClient.prototype.onRoomCreated = function(roomName){
		crc.room.roomName = roomName;
		enteredRoom('room created: ' + roomName);
	};
	
	ChatRoomClient.prototype.makeOffer = function(req){
		socket.emit('offer', crc.room.roomName, req);
	};
	
	ChatRoomClient.prototype.makeAnswer = function(sessDescription){
		socket.emit('answer', crc.room.roomName, sessDescription);
	};
	
	ChatRoomClient.prototype.emitIceCandidate = function(data){
		socket.emit('emitIceCandidate', crc.room.roomName, data);
	};
	
	ChatRoomClient.prototype.joinRoom = function(req){
		req['peerName'] = crc.peerName;
		socket.emit('joinRoom', req);
	};
	
	ChatRoomClient.prototype.joinConference = function(req){
		socket.emit('joinConference', req);
	};
	
	ChatRoomClient.prototype.getSignalChannel = function(data){
		return socket;
	};
	
	ChatRoomClient.prototype.onRoomFull = function(){
		showInfo('Room full');
	};
}