/// /////////////////////////////////////////////////////////
// - MediaHelper
// /////////////////////////////////////////////////////////

function MediaHelper(){
	var helper = this;

	helper.hasLocalStream = false;
	helper.localStream;
	helper.remoteStreams = {};
	helper.mediaConstraints = {video: true, audio: true};
	
	var onUserMediaSuccess = function(stream) {
	  helper.localStream = stream;
	  helper.hasLocalStream = true;
	  attachMediaStream(localVideo, stream);
	  
	  startMediaBtn.style.display = 'none';
	  hangUpBtn.style.display = 'block';
	};

	var onUserMediaError = function (error) {
	  messageError('Failed to get access to local media. Error code was ' +
				   error.code + '. Continuing without sending a stream.');
	  alert('Failed to get access to local media. Error code was ' +
			error.code + '. Continuing without sending a stream.');

	  helper.hasLocalStream = false;
	  
	  startMediaBtn.style.display = 'block';
	  hangUpBtn.style.display = 'none';
	};
	
	var __constructor = function() {
	}(this);
	
	MediaHelper.prototype.doGetUserMedia = function() {
	  // Call into getUserMedia via the polyfill (adapter.js).
	  try {
		getUserMedia(helper.mediaConstraints, onUserMediaSuccess,onUserMediaError);
	  } catch (e) {
		alert('getUserMedia() failed. Is this a WebRTC capable browser?');
		messageError('getUserMedia failed with exception: ' + e.message);
	  }
	}
	
	MediaHelper.prototype.addRemoteStream = function(peerId, stream){
		var video = document.createElement('VIDEO');
		video.setAttribute('autoplay', true);
		video.classList.add('remoteVideo');
		remotes.appendChild(video);
		helper.remoteStreams[peerId] = video;
		attachMediaStream(video, stream);
	};
	
	MediaHelper.prototype.removeRemoteStream = function(peerId, peerName){
		var video = helper.remoteStreams[peerId];
		video.parentNode.removeChild(video);
		delete helper.remoteStreams[peerId];
		showInfo(peerName + ' just left the chat.');
	};
}