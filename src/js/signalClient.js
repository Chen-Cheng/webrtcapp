/// /////////////////////////////////////////////////////////
// - AbstractSignalClient
//	 - Abstraction of signaling mechanism
// /////////////////////////////////////////////////////////

//TODO make it abstact
function SignalClient(){
	var sc = this;
	
	// Use io socket for signaling
	var socket = io.connect(),
		msgQueue = [],
		signalingReady = false;
	
	var __constructor  = function(){}(this);
	
	SignalClient.prototype.handleOffer = function(message){
	  // Add offer to the beginning of msgQueue, since we can't handle
      // Early candidates before offer at present.
      msgQueue.unshift(msg);
      // Callee creates PeerConnection
      signalingReady = true;
      maybeStart();	
	};
	
	//@abstract
	SignalClient.prototype.openChannel = function(){};
	
}